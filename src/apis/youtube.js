import axios from 'axios';

const KEY = 'AIzaSyC6xTMnSBRtB6Bvpsd9WxFcV89KyhMC0z4';


export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 6,
    key: KEY
  }
});
