import React from 'react';
import SearchBar from './SearchBar';
import youtube from '../apis/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
import ChangeButton from './ChangeButton';
import FlagList from './FlagList';

class App extends React.Component {
  state = {
    videos: [],
    selectedVideo: null,
    changeVideo: "change",
    flagSelected: "fr",
    flags: ["gb uk", "fr", "es"],
    TexteTranslated: ["Recherche de vidéo", "Souhaitez vous changer la vidéo lors d'une vidéo ou la laisser ?", "rester sur la vidéo", "changer la vidéo"]

  };

  componentDidMount() {
    this.onTermSubmit('la station saint omer')
  }
  onButtonClick = (event) => {
    if (this.state.changeVideo === "fixe") {
      this.setState({ changeVideo: "change" });
    } else if (this.state.changeVideo === "change") {
      this.setState({ changeVideo: "fixe" });
    };
  }

  onFlagClick = (event) => {
    let temporary = event.target.attributes.value.textContent;
    this.setState({ flagSelected: temporary });
    this.onLanguageChange(temporary);
  }
  onLanguageChange(e){
    if (e === "fr") {
      this.setState({ TexteTranslated: ["Recherche de vidéo", "Souhaitez vous changer la vidéo lors d'une recherche ou la laisser ?", "rester sur la vidéo", "changer la vidéo"]});
    } else if (e === "gb uk") {
      this.setState({ TexteTranslated: ["Search Video", "Do you want to change video when you start a search ?", "stay on the video", "change the video"]});
    } else if (e === "es") {
      this.setState({ TexteTranslated: ["búsqueda de video", "¿Desea cambiar el video durante una búsqueda o dejarlo?", "mantente en el video", "cambiar el video"]});
    }
  }
  onTermSubmit = async (term) => {
    const response = await youtube.get('/search', {
      params: {
        q: term
      }
    });
    if (this.state.changeVideo === "change") {
      for (var i = 0; i < response.data.items.length; i++) {
        if (response.data.items[i].id.videoId) {
          this.setState({
            videos: response.data.items,
            selectedVideo: response.data.items[i]
          });
          break;
        };
      };
    } else if (this.state.changeVideo === "fixe") {
      this.setState({
        videos: response.data.items,
      });
    }
  };

  onVideoSelect = (video) => {
    this.setState({ selectedVideo: video });
  };

  render() {
    return (
      <div className="ui container">
        <div className="ui row" style={{ marginBottom: '10px', marginTop: '5px'}}>
          <FlagList onFlagClick={this.onFlagClick} flagList={this.state.flags} flagSelected={this.state.flagSelected}  />
        </div>
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <SearchBar onFormSubmit={this.onTermSubmit} textSearchBar={this.state.TexteTranslated[0]}/>
            </div>
            <div className="five wide column">
              <ChangeButton onButtonClick={this.onButtonClick} position={this.state.changeVideo} textChangeButton={this.state.TexteTranslated}/>
            </div>
          </div>
        </div>
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail video={this.state.selectedVideo} />
            </div>
            <div className="five wide column">
              <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos} />
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default App;
