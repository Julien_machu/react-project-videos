import React from 'react';

const ChangeButton = (props) => {
  let nameBoutonClass = "ui primary basic button";
  let text = props.textChangeButton[2];

  if (props.position === "change") {
    nameBoutonClass = "ui primary basic button";
    text = props.textChangeButton[2];
  } else if (props.position === "fixe") {
    nameBoutonClass = "ui positive basic button";
    text = props.textChangeButton[3];
  }

    return (
      <div className="ui container">
        <p>{props.textChangeButton[1]}</p>
        <div className="ui large buttons">
          <button onClick={props.onButtonClick} className={nameBoutonClass} >{text}</button>
        </div>
      </div>
    );

}

export default ChangeButton;
