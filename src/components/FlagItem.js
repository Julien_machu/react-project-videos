import './FlagItem.css';
import React from 'react';

const FlagItem = (props) => {
  let temporary = "";
  if (props.flag === props.flagSelected) {
    temporary = "border-flag"
  }
  return (
    <div onClick={(event) => props.onFlagClick(event)} className={`item flag-item ${temporary}`} value={props.flag}>
      <i className={`flag ${props.flag}`} value={props.flag}></i>
    </div>
  );
};

export default FlagItem;
