import React from 'react';
import VideoItem from './VideoItem';

// const VideoList = ({videos}) =>{
// en écrivant la ligne du dessus, je peux replace les props.videos par juste videos
const VideoList = (props) => {
  const renderedList = props.videos.map((video) =>{
    if (!video.id.videoId) {
      return null;
    } else {
      return <VideoItem key={video.id.videoId} onVideoSelect={props.onVideoSelect} video={video} />;
    }
  });
  return (
    <div className="ui relaxed divided list"> {renderedList} </div>
  );
};

export default VideoList;
