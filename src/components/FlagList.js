import React from 'react';
import FlagItem from './FlagItem';


const FlagList = (props) => {
  const renderedFlagList = props.flagList.map((flag) =>{
      return <FlagItem flag={flag} key={flag} flagSelected={props.flagSelected} onFlagClick={props.onFlagClick} />;
    }
  );
  return (
    <div className="ui horizontal list">
      {renderedFlagList}
    </div>
  );
};

export default FlagList;
